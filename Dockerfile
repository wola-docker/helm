FROM alpine:3.12

RUN apk add --update --no-cache -t deps \
    ca-certificates \
    git \
    bash \
    openssl \
    curl \
    tar \
    libintl \
    gzip && \
    apk add --virtual build_deps gettext && \ 
    cp /usr/bin/envsubst /usr/local/bin/envsubst && \
    apk del build_deps && \
    rm -rf /var/cache/apk/* && \
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    mv kubectl /usr/bin/kubectl && chmod +x /usr/bin/kubectl && \
    curl -L https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh && \
    chmod 700 get_helm.sh && ./get_helm.sh --version v3.6.3 && \
    helm plugin install https://github.com/chartmuseum/helm-push.git  --version v0.9.0 && \
    helm repo add "stable" "https://charts.helm.sh/stable" --force-update
